#pragma once

#include "pipeline/pipelines.h"
#include "imaging/bitmap.h"
#include <memory>

using namespace raytracer;

namespace demos
{
	void my_demo(std::vector<Primitive> t, std::shared_ptr<raytracer::pipeline::Consumer<std::shared_ptr<imaging::Bitmap>>> output);
	
	
}