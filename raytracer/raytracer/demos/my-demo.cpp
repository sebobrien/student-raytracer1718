#include "demos/my-demo.h"
#include "demos/demo.h"
#include "util/lazy.h"
#include "easylogging++.h"

using namespace raytracer;
using namespace animation;
using namespace math;
using namespace imaging;
using namespace demos;
using namespace primitives;


namespace
{
	class MeshDemo : public Demo
	{
	public:
		MeshDemo(unsigned bitmap_size, animation::Duration duration, unsigned fps, unsigned antialias , std::vector<Primitive> triangles) : Demo(bitmap_size,duration,fps, antialias ), m_triangles(triangles)
		{
			
		}

	protected:
		/// <summary>
		/// Creates the root. This method will be called for several values of <paramref name="now" />,
		/// thus creating an animation.
		/// </summary>
		raytracer::Primitive create_root(TimeStamp now) override
		{
			// Local imports. Allows us to write sphere() instead of raytracer::primitives::sphere()
			using namespace raytracer::primitives;
			using namespace raytracer::materials;

			// Define material properties
			MaterialProperties material_properties(
				colors::red() *0.1,      // ambient lighting 
				colors::red() * 0.8,      // diffuse lighting
				colors::black() * 0.8,      // specular highlights
				10                   // specular exponent
			);
			MaterialProperties floor_material_properties(
				colors::white() *0.1,      // ambient lighting 
				colors::white() * 0.8,      // diffuse lighting
				colors::white() * 0,      // specular highlights
				0,                   // specular exponent
				0.5
			);
			// Create a uniform material: all parts of the primitive will be made out of the same material
			auto material = uniform(material_properties);
			std::vector<Primitive> primitives = {};
			std::vector<Primitive> primitives_t = {};

		/*	for (int i = -1; i <= 1; i++) {
				for (int j = -1; j <= 1; j++) {				
					primitives.push_back(decorate(material, translate(Point3D(i  , j  , 0) - Point3D(0, 0, 0), sphere())));
				}
			}*/
		/*	auto bba = hiearchical_bounding_box_accelerator(primitives);*/
			//for (int i = 1; i <m_triangles.size(); i++) {
			//	/*std::cout << i << "/" << m_triangles.size() << std::endl;
			//	std::cout << m_triangles[i]->bounding_box().center() << std::endl;*/
			//	primitives_t.push_back(decorate(material,m_triangles[i]));
			//	/*primitives_t.push_back(decorate(material, sphere()));*/
			//}
			//for(Primitive x : m_triangles) {
			//	
			//	/*primitives.push_back(decorate(material, translate(x->bounding_box().center() - Point3D(0, 0, 0), x)));*/
			//	/*primitives.push_back(decorate(material,primitives::triangle(Point3D(-1,0,0), Point3D(0, 1, 0), Point3D(1, 0, 0))));*/
			//	/*primitives.push_back(decorate(material, x));*/
			////};

			
			auto hbba = decorate(material , hiearchical_bounding_box_accelerator(m_triangles));
			/*auto hbba = hiearchical_bounding_box_accelerator(primitives_t);*/
			primitives.push_back(translate( (hbba->bounding_box().center()* -1) - Point3D(0, 0, 0) , hbba ) );
			primitives.push_back(decorate( uniform(floor_material_properties) , translate( Point3D(0, 0, 0) - Point3D(0,-1, 0),xz_plane())));
			auto root = make_union({primitives});
			
			/*auto b = root->bounding_box();
			auto bb = bba->bounding_box();
			return bba;*/
			return root;
			
		}

		/// <summary>
		/// Creates light sources.
		/// </summary>
		std::vector<raytracer::LightSource> create_light_sources(TimeStamp now) override
		{
			// Local import
			using namespace raytracer::lights;

			std::vector<LightSource> light_sources;

			light_sources.push_back(omnidirectional(Point3D(0, 2, 0) , colors::white() ));

			return light_sources;
		}

		/// <summary>
		/// Creates camera.
		/// </summary>
		raytracer::Camera create_camera(TimeStamp) override
		{
			return raytracer::cameras::perspective(
				Point3D(0, 0.20, 0.35),         // position of eye
				Point3D(0, 0, 0),          // point the camera looks at
				Vector3D(0, 1, 0),         // up-vector: indicates camera is "standing up"
				1,                         // distance between eye and viewing plane
				1                          // aspect ratio
			);
		}

	private:
		Primitive m_mesh;
		std::vector<Primitive> m_triangles;
	};
}

void demos::my_demo(std::vector<Primitive> t, std::shared_ptr<pipeline::Consumer<std::shared_ptr<Bitmap>>> output)
{
	
	MeshDemo(500, 1_s, 1, 1, t).render(output);
}
