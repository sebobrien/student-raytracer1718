#include "raytracers/ray-tracer-v2.h"
#include <iostream>  
#include "ray-tracer-v3.h"
#include <math.h> 
using namespace imaging;
using namespace math;
using namespace raytracer;







raytracer::RayTracer raytracer::raytracers::v3()
{
	
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV3>());
}

imaging::Color raytracer::raytracers::_private_::RayTracerV3::process_light_ray(const Scene & scene, const MaterialProperties & mp , const Hit & hit, const math::Ray & ray, const LightRay & Light_ray) const
{
	
	imaging::Color result = RayTracerV2::process_light_ray(scene,mp,hit,ray,Light_ray);
	
	result += compute_specular(mp, hit, ray, Light_ray);
	
	return result;
}

imaging::Color raytracer::raytracers::_private_::RayTracerV3::compute_specular(const MaterialProperties & mp, const Hit & hit, const math::Ray & ray, const LightRay & lr) const
{
	if (mp.specular != colors::black()) {
		Point3D L = lr.ray.origin;
		Color Cl = lr.color;
		Point3D P = hit.position;
		Color Cp = mp.specular;
		Point3D E = ray.origin;
		
		Vector3D i = (P - L).normalized();
		Vector3D r = i.reflect_by(hit.normal);
		Vector3D v = (E - P).normalized();
		double e = mp.specular_exponent;
		double cos = v.dot(r);
		
		if (cos > 0) {
			return Cl * Cp * pow(cos, e);
		}
		
	}
	return colors::black();
}
