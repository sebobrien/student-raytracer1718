#pragma once

#include <math.h>
#include "raytracers/ray-tracer-v5.h"
#include <memory>


namespace raytracer
{
	namespace raytracers
	{
		namespace _private_
		{
			class RayTracerV6 : public RayTracerV5
			{


			protected:
				TraceResult trace(const Scene&, const math::Ray&, double weight) const override;
				imaging::Color compute_refraction(const Scene & scene, const MaterialProperties & mp, const Hit & hit, const math::Ray & ray, double weight) const;

			};
		}


		RayTracer v6();
	}
}
