#include "raytracers/ray-tracer-v3.h"
#include <iostream>  
#include "ray-tracer-v4.h"
#include <math.h> 

using namespace imaging;
using namespace math;
using namespace raytracer;
using namespace std;







raytracer::RayTracer raytracer::raytracers::v4()
{
	
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV4>());
}

imaging::Color raytracer::raytracers::_private_::RayTracerV4::process_light_ray(const Scene & scene, const MaterialProperties & mp , const Hit & hit, const math::Ray & ray, const LightRay & Light_ray) const
{	
	Hit hit_scene;

	if (scene.root->find_first_positive_hit(Light_ray.ray, &hit_scene))
	{
		if (hit_scene.t < 0.9999 && hit_scene.t >= 0) {

			return colors::black();
		}
	}

	
	

	imaging::Color result = RayTracerV3::process_light_ray(scene,mp,hit,ray,Light_ray);	
		
	return result;
}


