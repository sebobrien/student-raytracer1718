#include "raytracers/ray-tracer-v3.h"
#include "ray-tracer-v2.h"
#include <iostream>  
#include "ray-tracer-v4.h"
#include <math.h> 
#include "ray-tracer-v5.h"

using namespace imaging;
using namespace math;
using namespace raytracer;
using namespace std;







raytracer::RayTracer raytracer::raytracers::v5()
{

	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV5>());
}

TraceResult raytracer::raytracers::_private_::RayTracerV5::trace(const Scene& scene, const Ray& ray, double weight) const
{
	Hit hit;

	// Ask the scene for the first positive hit, i.e. the closest hit in front of the eye
	// If there's a hit, find_first_positive_hit returns true and updates the hit object with information about the hit
	if (weight > 0.01 && scene.root->find_first_positive_hit(ray, &hit))
	{
		// There's been a hit
		// Fill in TraceResult object with information about the trace


		
			// This ray tracer returns the materialPropertie ambient colour in case of a hit
			MaterialProperties mp = hit.material->at(hit.local_position);
			Color result = colors::black();
			result += compute_ambient(mp);
			result += process_lights(scene, mp, hit, ray);
			result += compute_reflection(scene, mp, hit, ray, weight);
			// The hit object contains the group id, just copy it (group ids are important for edge detection)
			unsigned group_id = hit.group_id;

			// The t-value indicates where the ray/scene intersection took place.
			// You can use ray.at(t) to find the xyz-coordinates in space.
			double t = hit.t;

			// Group all this data into a TraceResult object.
			return TraceResult(result, group_id, ray, t);
	
		
	}
	else
	{
		// The ray missed all objects in the scene
		// Return a TraceResult object representing "no hit found"
		// which is basically the same as returning black
		return TraceResult::no_hit(ray);
	}
}


TraceResult raytracer::raytracers::_private_::RayTracerV5::trace(const Scene & scene, const math::Ray & ray) const
{
	return trace(scene, ray, 1.0);
}

//imaging::Color raytracer::raytracers::_private_::RayTracerV5::process_light_ray(const Scene & scene, const MaterialProperties & mp, const Hit & hit, const math::Ray & ray, const LightRay & Light_ray,double weight) const
//{
//
//	imaging::Color result = RayTracerV4::process_light_ray(scene, mp, hit, ray, Light_ray);
//
//	result += compute_reflection(mp, hit, ray, Light_ray, weight);
//
//	return result;
//}

imaging::Color raytracer::raytracers::_private_::RayTracerV5::compute_reflection(const Scene & scene, const MaterialProperties & mp, const Hit & hit, const math::Ray & ray, double weight) const
{
	if (mp.reflectivity) {
		Point3D E = ray.origin;
		Point3D P = hit.position;
		
		Vector3D i = (P - E).normalized();

		Vector3D direction = i.reflect_by(hit.normal);
		
		
		auto R_r = Ray(P + (0.00000001*direction), direction);

		return mp.reflectivity * trace(scene, R_r, weight * mp.reflectivity).color;
	}
	else return colors::black();
}
