#pragma once

#include "raytracers/ray-tracer-v4.h"
#include "raytracers/ray-tracer-v0.h"
#include <memory>


namespace raytracer
{
	namespace raytracers
	{
		namespace _private_
		{
			class RayTracerV5 : public RayTracerV4
			{


			protected:
				virtual TraceResult trace(const Scene&, const math::Ray&, double weight) const ;
				TraceResult trace(const Scene&, const math::Ray&) const override;
			/*	virtual imaging::Color process_light_ray(const Scene&, const MaterialProperties&, const Hit&, const math::Ray&, const LightRay&, double weight) const ;*/
				virtual imaging::Color  compute_reflection(const Scene&, const MaterialProperties&, const Hit&, const math::Ray&, double weight) const;

			};
		}


		RayTracer v5();
	}
}