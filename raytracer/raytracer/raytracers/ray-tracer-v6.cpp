#include "ray-tracer-v6.h"

using namespace imaging;
using namespace math;
using namespace raytracer;
using namespace std;


TraceResult raytracer::raytracers::_private_::RayTracerV6::trace(const Scene& scene, const Ray& ray, double weight) const
{
	Hit hit;

	// Ask the scene for the first positive hit, i.e. the closest hit in front of the eye
	// If there's a hit, find_first_positive_hit returns true and updates the hit object with information about the hit
	if (weight > 0.01 && scene.root->find_first_positive_hit(ray, &hit))
	{
		// There's been a hit
		// Fill in TraceResult object with information about the trace



		// This ray tracer returns the materialPropertie ambient colour in case of a hit
		MaterialProperties mp = hit.material->at(hit.local_position);
		Color result = colors::black();
		result += compute_ambient(mp);
		result += process_lights(scene, mp, hit, ray);
		result += compute_reflection(scene, mp, hit, ray, weight);
		result += compute_refraction(scene, mp, hit, ray, weight);
		// The hit object contains the group id, just copy it (group ids are important for edge detection)
		unsigned group_id = hit.group_id;

		// The t-value indicates where the ray/scene intersection took place.
		// You can use ray.at(t) to find the xyz-coordinates in space.
		double t = hit.t;

		// Group all this data into a TraceResult object.
		return TraceResult(result, group_id, ray, t);


	}
	else
	{
		// The ray missed all objects in the scene
		// Return a TraceResult object representing "no hit found"
		// which is basically the same as returning black
		return TraceResult::no_hit(ray);
	}
}

imaging::Color raytracer::raytracers::_private_::RayTracerV6::compute_refraction(const Scene & scene, const MaterialProperties & mp, const Hit & hit, const math::Ray & ray, double weight) const
{
	if (mp.transparency > 0) {
		// Ray enters transparent object, compute how it is bent at point P1
		Point3D E = ray.origin;
		Point3D P = hit.position;
		double n2 = mp.refractive_index;
		Vector3D n = hit.normal;
		
		Vector3D i = (P - E).normalized();	
		Vector3D o_x = 1 / n2 * (i - (i.dot(n)) * n) ;
		Vector3D o_y =  ( - ( sqrt(1 - abs(o_x.dot(o_x)) ) ) ) * n ;
		Vector3D out_direction = (o_x + o_y);

		auto R_r = Ray(P + (0.00000001*out_direction), out_direction);

		if (1 - abs(o_x.dot(o_x)) < 0) {
			return colors::black();
		}

		// Find exit point P2
		Hit exit_hit; 		
		
		if (!scene.root->find_first_positive_hit(R_r, &exit_hit)) {
			// no exit point
		  return colors::black();
		}
		Point3D E2 = P;
		Point3D P2 = exit_hit.position;
		double n1_2 = exit_hit.material->at(exit_hit.local_position).refractive_index;
		double n2_2 = 1;
		Vector3D n_p2 = exit_hit.normal;
		
		Vector3D i2 = (P2 - E2).normalized();
		Vector3D o_x2 = n1_2 / n2_2 * (i2 - (i2.dot(n_p2)) * n_p2);
		Vector3D o_y2 = (-(sqrt(1 - abs(o_x2.dot(o_x2))))) * n_p2;
		Vector3D out_direction_2 = ( o_x2 + o_y2);
		
		auto exit_r = Ray(P2 + (0.00000001*out_direction_2), out_direction_2);

		return mp.transparency * trace(scene, exit_r, weight * mp.transparency).color;


	}
	else return colors::black();
}

raytracer::RayTracer raytracer::raytracers::v6()
{

	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV6>());
}

