#include "raytracers/ray-tracer-v2.h"
#include <iostream>  
using namespace imaging;
using namespace math;
using namespace raytracer;


TraceResult raytracer::raytracers::_private_::RayTracerV2::trace(const Scene& scene, const Ray& ray) const
{
	Hit hit;

	// Ask the scene for the first positive hit, i.e. the closest hit in front of the eye
	// If there's a hit, find_first_positive_hit returns true and updates the hit object with information about the hit
	if (scene.root->find_first_positive_hit(ray, &hit))
	{
		// There's been a hit
		// Fill in TraceResult object with information about the trace

		

		// This ray tracer returns the materialPropertie ambient colour in case of a hit
		MaterialProperties mp = hit.material->at(hit.local_position);
		Color result = colors::black();
		result += compute_ambient(mp);
		result += process_lights(scene, mp, hit, ray);
		// The hit object contains the group id, just copy it (group ids are important for edge detection)
		unsigned group_id = hit.group_id;

		// The t-value indicates where the ray/scene intersection took place.
		// You can use ray.at(t) to find the xyz-coordinates in space.
		double t = hit.t;

		// Group all this data into a TraceResult object.
		return TraceResult(result, group_id, ray, t);
	}
	else
	{
		// The ray missed all objects in the scene
		// Return a TraceResult object representing "no hit found"
		// which is basically the same as returning black
		return TraceResult::no_hit(ray);
	}
}



Color raytracer::raytracers::_private_::RayTracerV2::process_lights(const Scene& scene, const MaterialProperties& mp, const Hit& hit, const Ray& ray) const
{
	Color result = colors::black();
	
	for each (LightSource ls in scene.light_sources) {
		result += process_light_source(scene, mp, hit, ray, ls);
	}
	return result;
}

Color raytracer::raytracers::_private_::RayTracerV2::process_light_source(const Scene& scene, const MaterialProperties& mp, const Hit& hit, const Ray& ray, LightSource light_source) const
{
	Color result = colors::black();

	for each (LightRay lr in light_source->lightrays_to(hit.position)) {
		result += process_light_ray(scene, mp, hit, ray, lr);
	}
	return result;
}

Color raytracer::raytracers::_private_::RayTracerV2::process_light_ray(const Scene& scene, const MaterialProperties& mp, const Hit& hit, const Ray& ray, const LightRay& light_ray) const
{
	
	return colors::black() += compute_diffuse(mp, hit, ray, light_ray);
}

Color raytracer::raytracers::_private_::RayTracerV2::compute_diffuse(const MaterialProperties & mp, const Hit & hit, const Ray & ray, const LightRay & light_ray) const
{
	
		double cos = (light_ray.ray.origin - hit.position).normalized().dot(hit.normal);
		if (cos > 0) {
			
			return cos * light_ray.color * mp.diffuse;
			
			
		}

		/*std::cout << cos;*/
			return colors::black();
	
}






raytracer::RayTracer raytracer::raytracers::v2()
{
	
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV2>());
}