#include "lights/directional-light.h"

using namespace math;
using namespace raytracer;

namespace
{
	
	class DirectionalLight : public raytracer::lights::_private_::LightSourceImplementation
	{
	public:
		DirectionalLight(const math::Vector3D direction, const imaging::Color& color)
			: m_direction(direction), m_color(color) { }

	protected:
		std::vector<LightRay> lightrays_to(const math::Point3D& p) const override
		{
			std::vector<LightRay> result;
			
			math::Ray ray ( p-m_direction , p);
			result.push_back(LightRay(ray, m_color));
			return result;
			
		}

	private:
		math::Vector3D m_direction;
	    imaging::Color m_color;
	};
}

LightSource raytracer::lights::directional(const math::Vector3D direction, const imaging::Color& color)
{
	return LightSource(std::make_shared<DirectionalLight>(direction, color));
}
