#include "lights/spot-point-light.h"

using namespace math;
using namespace raytracer;

namespace
{
	/// <summary>
	/// Implementation for omnidirectional point lights.
	/// </summary>
	class SpotPointLight : public raytracer::lights::_private_::PointLightImplementation
	{
	public:
		SpotPointLight(const math::Point3D& position, const math::Point3D& direction, Angle angle, const imaging::Color& color)
			: PointLightImplementation(position), m_color(color), m_direction(direction), m_angle(angle) { }

	protected:
		LightRay cast_lightray_to(const math::Point3D& p) const override
		{
			math::Ray ray(m_position, p);
			
			Point3D L = m_position;
			Vector3D v = (m_direction - m_position).normalized();
			
			if ((p - L).normalized().dot(v) >= cos(m_angle / 2))
			{

				return LightRay(ray, m_color);
			}
			return LightRay(ray, imaging::colors::black());

		}

	private:
		imaging::Color m_color;
		math::Point3D m_direction;
		math::Angle m_angle;
	};
}

LightSource raytracer::lights::spot(const math::Point3D& position, const Point3D& direction, const Angle& angle, const imaging::Color& color)
{
	return LightSource(std::make_shared<SpotPointLight>(position, direction, angle, color));
}
