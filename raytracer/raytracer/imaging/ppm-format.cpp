#include "ppm-format.h"
#include <iostream>

void imaging::PPM::write_text_ppm(const Bitmap & bitmap, std::ostream & out)
{
	/*out << "P3" << " " << 4 << " " << 4 << " " << 15 << std::endl <<
		"0  0  0    0  0  0    0  0  0   15  0 15" << std::endl
		<< "0  0  0    0 15  7    0  0  0    0  0  0" << std::endl
		<< "0  0  0    0  0  0    0 15  7    0  0  0" << std::endl
		<< "15  0 15    0  0  0    0  0  0    0  0  0 " << std::endl;*/
	
	out << "P3" << " " << bitmap.width() << " " << bitmap.height() << " " << 255 << std::endl;
	
	
	for (unsigned y = 0; y != bitmap.height(); ++y)
	{
		for (unsigned x = 0; x != bitmap.width(); ++x)
		{	
			
			
			Color rgb = bitmap[Position2D(x, y)];
			Color rgbC = rgb.clamped();
			
			out << std::round(rgbC.r * 255) << " "  << std::round(rgbC.g * 255) << " " << std::round(rgbC.b * 255) << " ";
			
			
		}
		out << std::endl;
	}

	/*bitmap.for_each_position([bitmap,out](const Position2D& position) {
		out << (bitmap)[position].r() + " " + (bitmap)[position].b() + " " + (bitmap)[position].g();
		
	});*/
}
