#pragma once

#include "circular-animation.h"
#include "animation/animation.h"
#include "math/interval.h"
#include "animation/interval-animation.h"

using namespace math;



namespace animation
{

	Animation<math::Point3D> CircularImplementation(const math::Point3D& point, const math::Point3D& center, const math::Vector3D& rotation_axis, const math::Interval<math::Angle>& angle_interval, const Duration& duration)
	{

		/*auto double_animation = basic(angle_interval.lower.degrees, angle_interval.upper.degrees, duration);*/
		auto angle_animation = animate(angle_interval.lower, angle_interval.upper, duration);
		auto double_animation = basic(0, 1, duration);
		double R = distance(point,center);
		std::function<math::Point3D(TimeStamp)> lambda;
		if (rotation_axis == math::Vector3D(0, 0, 1)) {

			lambda = [double_animation, angle_animation, point,center, R](TimeStamp now) -> math::Point3D {
				auto t = angle_animation(now);
				double x_ = center.x() + cos(t)*R + point.x();
				double y_ = center.y() + sin(t)*R + point.y();
				return Point3D(x_, y_, point.z());
			};
		}
		if (rotation_axis == math::Vector3D(1, 0, 0)) {

			lambda = [double_animation, angle_animation, point](TimeStamp now) -> math::Point3D {


				auto t = angle_animation(now);

				return Point3D(point.x(), sin(t),cos(t));
			};
		}
		if (rotation_axis == math::Vector3D(0, 1, 0)) {

			lambda = [double_animation, angle_animation, point](TimeStamp now) -> math::Point3D {


				auto t = angle_animation(now);

				return Point3D(sin(t),point.y(), cos(t));
			};


		};

		// Turns the function into an Animation object
		return make_animation(math::from_lambda(lambda), duration);
	}

	Animation<math::Point3D> circular(const math::Point3D& point, const math::Point3D& center, const math::Vector3D& rotation_axis, const math::Interval<math::Angle>& angle_interval, const Duration& duration) {

		return CircularImplementation(point, center, rotation_axis, angle_interval, duration);

	}
}