#include "hiearchical-bounding-box-accelerator.h"
#include "primitives/union-primitive.h"
#include "primitives/bounding-box-accelerator.h"


using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;
using namespace std;

namespace
{
	class HiearchicalBoundingBoxAcceleratorImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{
	protected:
		

	public:

		math::Box box;
		Primitive m_left_node;
		Primitive m_right_node;
		bool leaf;
		int depth;

		HiearchicalBoundingBoxAcceleratorImplementation(std::vector<Primitive>& children, int depth)
			: box(math::Box::empty()), depth(depth)
		{
			make_tree(children);
		}

		void make_tree(std::vector<Primitive>& children) {

			if (children.size() > 6) {
			/*if (depth > 0) {*/
				//merge all boxes of children in one large box
				math::Box box_children = math::Box::empty();
				for (int i = 0; i < children.size(); i++) {


					box_children = children.at(i)->bounding_box().merge(box_children);

				}
				
				
				//split list of children into two lists by position to largest axis of box_children
				std::vector<Primitive> left_list;
				std::vector<Primitive> right_list;

				auto x = box_children.x();
				auto y = box_children.y();
				auto z = box_children.z();

				auto v = std::vector<Interval<double>>({ x,y,z });
				Interval<double> largest_axis = (*(std::max_element(v.begin(), v.end(),
					[](Interval<double> a, Interval<double> b) { return a.size() < b.size(); })));
				
				math::Box left_box = math::Box::empty();
				math::Box right_box = math::Box::empty();

				// sort into left or right lists
				if (x.size() == largest_axis.size()) {
					left_box = Box(nonempty_interval(x.lower, (x.lower + x.upper) / 2), y, z);
					right_box = Box(nonempty_interval((x.lower + x.upper) / 2, x.lower), y, z);
					for (Primitive p : children) {
						if (left_box.x().upper >= p->bounding_box().center().x() && left_box.x().lower <= p->bounding_box().center().x())
						{

							
							
							left_list.push_back(p);
						}
						else { right_list.push_back(p); }
					}
				}
				else

					if (y.size() == largest_axis.size()) {
						left_box = Box(x, nonempty_interval(y.lower, (y.lower + y.upper) / 2), z);
						right_box = Box(x, nonempty_interval((y.lower + y.upper) / 2, y.lower), z);
						for (Primitive p : children) {
							if (left_box.y().upper >= p->bounding_box().center().y() && left_box.y().lower <= p->bounding_box().center().y())
							{
								
								left_list.push_back(p);
							}
							else { right_list.push_back(p); }
						}
					}
					else


						if (z.size() == largest_axis.size()) {
							left_box = Box(x, y, nonempty_interval(z.lower, (z.lower + z.upper) / 2));
							right_box = Box(x, y, nonempty_interval((z.lower + z.upper) / 2, z.lower));
							for (Primitive p : children) {
								if (left_box.z().upper >= p->bounding_box().center().z() && left_box.z().lower <= p->bounding_box().center().z())
								{
									
									left_list.push_back(p);
								}
								else { right_list.push_back(p); }
							}
					}
				
				
				
				
				box = box_children;
				/*std::cout << "6" << ":" << children.size() << std::endl;*/
				m_left_node = Primitive(std::make_shared<HiearchicalBoundingBoxAcceleratorImplementation>(left_list, depth - 1));
				m_right_node = Primitive(std::make_shared<HiearchicalBoundingBoxAcceleratorImplementation>(right_list, depth - 1));
				leaf = false;
			}

			else {
				
				auto leaf_union = make_union(children);
				leaf = true;
				//m_left_node = children[0];
				m_left_node = leaf_union;
				box = m_left_node->bounding_box();
			}
						
		}

		bool find_first_positive_hit(const Ray& ray, Hit* hit) const override
		{
			bool hitleft;
			bool hitright;
			if (box.is_hit_by(ray)) {
				if (leaf) {
					return m_left_node->find_first_positive_hit(ray, hit);
				}
				bool hitleft = m_left_node->find_first_positive_hit(ray, hit);
				bool hitright = m_right_node->find_first_positive_hit(ray, hit);
				return hitleft || hitright;
			}
		}

		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{
			std::vector<std::shared_ptr<Hit>> result;
			if(box.is_hit_by(ray)){
			
				if (leaf) {
					return m_left_node->find_all_hits(ray);
				}

				std::vector<std::shared_ptr<Hit>> left = m_left_node->find_all_hits(ray);			
				std::vector<std::shared_ptr<Hit>> right = m_right_node->find_all_hits(ray);

				/*for (int i = 0; i < left.size() || right.size(); i++) {
					auto left_hit = left.at(i);
					auto right_hit = right.at(i);
					
					if (left_hit->t <= right_hit->t) {
						result.push_back(left_hit);
					} else if (left_hit->t < right_hit->t) {
						result.push_back(right_hit);
					}
					
				}*/
				result.insert(result.end(), left.begin(), left.end());
				result.insert(result.end(), right.begin(), right.end());
			
			}
			


			return result;
		}

		math::Box bounding_box() const override {
			return box;
		}





	};


}


Primitive raytracer::primitives::hiearchical_bounding_box_accelerator(std::vector<Primitive>& children)
{
	return Primitive(std::make_shared<HiearchicalBoundingBoxAcceleratorImplementation>(children, 5));



}


