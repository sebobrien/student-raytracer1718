#include "bounding-box-accelerator.h"


using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;
using namespace std;

namespace
{
	class BoundingBoxAcceleratorImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{
	protected:
		

		

	public:
		const Primitive m_child;
		const math::Box m_child_box;

		BoundingBoxAcceleratorImplementation(const Primitive primitive)
			: m_child(primitive), m_child_box(primitive->bounding_box())
		{
		}

		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{

			std::vector<std::shared_ptr<Hit>> hits;
			if (m_child_box.is_hit_by(ray)) {
				hits = m_child->find_all_hits(ray);
			};
			return hits;
		}

		math::Box bounding_box() const override {
			return m_child_box;
		}

	
		


	};


}


Primitive raytracer::primitives::bounding_box_accelerator(const Primitive primitive)
{
	return Primitive(std::make_shared<BoundingBoxAcceleratorImplementation>(primitive));
}
