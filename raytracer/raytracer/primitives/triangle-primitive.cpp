#include "primitives/triangle-primitive.h"
#include <assert.h>

using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;


namespace
{
	class TriangleImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{
	protected:
		

	public:
		const Point3D m_p1;
		const Point3D m_p2;
		const Point3D m_p3;
		const Vector3D m_normal;

		TriangleImplementation(const Point3D p1, const Point3D p2, const Point3D p3)
			: m_p1(p1), m_p2(p2), m_p3(p3), m_normal((((p2 - p1).cross(p3 - p1)).normalized()))
		{
			
			
		}

		

		void initialize_hit(Hit* hit, const Ray& ray, double t) const
		{
			// Update Hit object
			hit->t = t;
			hit->position = ray.at(t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.x(), hit->position.y());
			hit->normal = ray.origin.z() > 0 ? m_normal : -m_normal;


		}

		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{	
			
			std::vector<std::shared_ptr<Hit>> hits;
			/*Vector3D normal = ((Point3D(0,0,0) - Point3D(0, 0, 0)).cross(Point3D(0, 0, 0) - Point3D(0, 0, 0))).normalized;*/
			double denom = ray.direction.dot(m_normal);
			
			if (denom != approx(0.0))
			{
				double numer = (m_p1 - ray.origin).dot(m_normal);
				double t = numer / denom;
								
				auto hit = std::make_shared<Hit>();							
				initialize_hit(hit.get(), ray, t);
				Point3D h = hit->position;
				double p1_p2 = (m_p2 - m_p1).cross(h - m_p1).dot(m_normal);
				double p3_p2 = (m_p3 - m_p2).cross(h - m_p2).dot(m_normal);
				double p3_p1 = (m_p1 - m_p3).cross(h - m_p3).dot(m_normal);
				if (p1_p2 >= 0 && p3_p2 >= 0 && p3_p1 >= 0) {

					hits.push_back(hit);
				}
			}
			
			return hits;
		}

		bool find_first_positive_hit(const math::Ray& ray, Hit* hit) const override
		{	
			
			assert(hit);				
			double denom = ray.direction.dot(m_normal);
			
			if (denom != approx(0.0))
			{
				
					// Compute numerator
					double numer = (m_p1 - ray.origin).dot(m_normal);

					// Compute t
					double t = numer / denom;

					if (t > 0) {

						if (hit->t && t < hit->t)
						{
							double numer = (m_p1 - ray.origin).dot(m_normal);
							double t = numer / denom;
							Point3D h = ray.at(t);
							double p1_p2 = (m_p2 - m_p1).cross(h - m_p1).dot(m_normal);
							double p3_p2 = (m_p3 - m_p2).cross(h - m_p2).dot(m_normal);
							double p3_p1 = (m_p1 - m_p3).cross(h - m_p3).dot(m_normal);
							if (p1_p2 >= 0 && p3_p2 >= 0 && p3_p1 >= 0) {
								initialize_hit(hit, ray, t);
								return true;
							}

						}

					}
		
			}// No positive hits were found
			return false;
		}

		math::Box bounding_box() const override
		{	
			double min_x = std::min(std::min(m_p1.x(), m_p2.x() ), m_p3.x() );
			double max_x = std::max(std::max(m_p1.x(), m_p2.x() ), m_p3.x() );
			double min_y = std::min(std::min(m_p1.y(), m_p2.y() ), m_p3.y() );
			double max_y = std::max(std::max(m_p1.y(), m_p2.y() ), m_p3.y() );
			double min_z = std::min(std::min(m_p1.z(), m_p2.z() ), m_p3.z() );
			double max_z = std::max(std::max(m_p1.z(), m_p2.z()), m_p3.z());
			return Box(interval(min_x, max_x), interval(min_y, max_y), interval(min_z, max_z)  );
		}
		
	};
}

Primitive raytracer::primitives::triangle(const Point3D p1, const Point3D p2, const Point3D p3)
{
	return Primitive(std::make_shared<TriangleImplementation>(p1,p2,p3));
}


