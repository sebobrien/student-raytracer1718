#include "primitives/circle-primitive.h"
#include "math/interval.h"
#include <windows.h>

using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;


namespace
{


	class CoordinateCircleImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{

	protected:
		const Vector3D m_normal;
		

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="normal">
		/// Normal vector on plane. Needs to have unit length.
		/// </param>
		CoordinateCircleImplementation(const Vector3D& normal, const double& length)
			: m_normal(normal)
		{
			assert(normal.is_unit());
		}

		virtual void initialize_hit(Hit* hit, const Ray& ray, double t) const = 0;

	public:
		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{
			std::vector<std::shared_ptr<Hit>> hits;

			// Compute denominator
			double denom = ray.direction.dot(m_normal);

			// If denominator == 0, there is no intersection (ray runs parallel to plane)
			if (denom != approx(0.0))
			{
				// Compute numerator
				double numer = -((ray.origin - Point3D(0, 0, 0)).dot(m_normal));

				// Compute t
				double t = numer / denom;

				// Create hit object
				auto hit = std::make_shared<Hit>();

				// shared_ptr<T>::get() returns the T* inside the shared pointer
				initialize_hit(hit.get(), ray, t);

				Point3D hit_posistion = hit->position;
				
				

				Point3D p = hit->position;
				 if (m_normal == Vector3D(0, 0, 1)){
					 double p_distance = sqrt(pow(p.x(), 2) + pow(p.y(), 2));
					 if (p_distance <= 1) {

						 // Put hit in list
						 hits.push_back(hit);

					 }
				 }
				 else if (m_normal == Vector3D(1, 0, 0)) {
					 double p_distance = sqrt(pow(p.z(), 2) + pow(p.y(), 2));
					 if (p_distance <= 1) {

						 // Put hit in list
						 hits.push_back(hit);

					 }
				 }
				 else if (m_normal == Vector3D(0, 1, 0)) {
					 double p_distance = sqrt(pow(p.z(), 2) + pow(p.x(), 2));
					 if (p_distance <= 1) {

						 // Put hit in list
						 hits.push_back(hit);

					 }
				 }
				
				
			}

			return hits;
		}

		bool find_first_positive_hit(const math::Ray& ray, Hit* hit) const override
		{
			assert(hit);


			// Compute denominator
			double denom = ray.direction.dot(m_normal);

			// If denominator == 0, there is no intersection (ray runs parallel to plane)
			if (denom != approx(0.0))
			{
				// Compute numerator
				double numer = -((ray.origin - Point3D(0, 0, 0)).dot(m_normal));

				// Compute t
				double t = numer / denom;


				if (t > 0) {

					// Check that our new t is better than the pre-existing t

					if (t <= hit->t)
					{

						
						



						Point3D p = ray.at(t);
						if (m_normal == Vector3D(0, 0, 1)) {
							double p_distance = sqrt(pow(p.x(), 2) + pow(p.y(), 2));
							if (p_distance <= 1) {
								initialize_hit(hit, ray, t);
								return true;

							}
						}
						else if (m_normal == Vector3D(1, 0, 0)) {
							double p_distance = sqrt(pow(p.z(), 2) + pow(p.y(), 2));
							if (p_distance <= 1) {

								initialize_hit(hit, ray, t);
								return true;

							}
						}
						else if (m_normal == Vector3D(0, 1, 0)) {
							double p_distance = sqrt(pow(p.z(), 2) + pow(p.x(), 2));
							if (p_distance <= 1) {

								initialize_hit(hit, ray, t);
								return true;

							}
						}


					}

				}
			}
			
			// No positive hits were found
			return false;
		}
	};

	class CircleXYImplementation : public CoordinateCircleImplementation
	{
	public:
		CircleXYImplementation()
			: CoordinateCircleImplementation(Vector3D(0, 0, 1), 2.0)
		{
			// NOP
		}

		math::Box bounding_box() const override
		{
			return Box(Interval<double>::infinite(), Interval<double>::infinite(), interval(-0.01, 0.01));
		}

	protected:
		void initialize_hit(Hit* hit, const Ray& ray, double t) const override
		{
			hit->t = t;
			hit->position = ray.at(t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.x(), hit->position.y());
			hit->normal = ray.origin.z() > 0 ? m_normal : -m_normal;
		}
	};

	class CircleXZImplementation : public CoordinateCircleImplementation
	{
	public:
		CircleXZImplementation()
			: CoordinateCircleImplementation(Vector3D(0, 1, 0), 2.0)
		{
			// NOP
		}

		math::Box bounding_box() const override
		{
			return Box(Interval<double>::infinite(), interval(-0.01, 0.01), Interval<double>::infinite());
		}

	protected:
		void initialize_hit(Hit* hit, const Ray& ray, double t) const override
		{
			hit->t = t;
			hit->position = ray.at(t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.x(), hit->position.z());
			hit->normal = ray.origin.y() > 0 ? m_normal : -m_normal;
		}
	};

	class CircleYZImplementation : public CoordinateCircleImplementation
	{
	public:
		CircleYZImplementation()
			: CoordinateCircleImplementation(Vector3D(1, 0, 0), 2.0)
		{
			// NOP
		}

		math::Box bounding_box() const override
		{
			return Box(interval(-0.01, 0.01), Interval<double>::infinite(), Interval<double>::infinite());
		}

	protected:
		void initialize_hit(Hit* hit, const Ray& ray, double t) const override
		{
			hit->t = t;
			hit->position = ray.at(t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.y(), hit->position.z());
			hit->normal = ray.origin.x() > 0 ? m_normal : -m_normal;
		}
	};
}






Primitive raytracer::primitives::xy_circle()
{
	return Primitive(std::make_shared<CircleXYImplementation>());
}

Primitive raytracer::primitives::xz_circle()
{
	return Primitive(std::make_shared<CircleXZImplementation>());
}

Primitive raytracer::primitives::yz_circle()
{
	return Primitive(std::make_shared<CircleYZImplementation>());
}
