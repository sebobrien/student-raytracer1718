#pragma once

#include "primitives/primitive.h"
#include "math/point.h"

using namespace math;

namespace raytracer
{
	namespace primitives
	{
		
		Primitive triangle(const Point3D p1, const Point3D p2, const Point3D p3);
		
	}
}
