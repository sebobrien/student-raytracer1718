#include "mesh.h"
#include "mesh/mesh-reader.h"
#include "primitives/hiearchical-bounding-box-accelerator.h"

using namespace raytracer;
using namespace raytracer::primitives;

using namespace std;

namespace
{
	class MeshImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{
	protected:
	
		Primitive children;

	public:


		MeshImplementation(const std::string& filename) :
			children(hiearchical_bounding_box_accelerator(mesh::MeshReader(filename).read_ply()))
		{
			/*children = hiearchical_bounding_box_accelerator(mesh::MeshReader(filename).read_ply());*/
		}

		bool find_first_positive_hit(const math::Ray& ray, Hit* hit) const override
		{
			return children->find_first_positive_hit(ray, hit);
		}

		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{
			
			return children->find_all_hits(ray);
		}

		math::Box bounding_box() const override {
			return children->bounding_box();
		}

	};


}


Primitive raytracer::primitives::mesh(const std::string& path)
{
	
	
	return Primitive(std::make_shared<MeshImplementation>(path));

}


