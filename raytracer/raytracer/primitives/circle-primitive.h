#pragma once

#include "primitives/primitive.h"


namespace raytracer
{
	namespace primitives
	{
		Primitive xy_circle();
		Primitive xz_circle();
		Primitive yz_circle();
	}
}
