#include "primitives/square-primitive.h"
#include "math/interval.h"
#include <windows.h>

using namespace raytracer;
using namespace raytracer::primitives;
using namespace math;


namespace
{


	class SquareImplementation : public raytracer::primitives::_private_::PrimitiveImplementation
	{

	protected:
		const Vector3D m_normal;
		const double m_length;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="normal">
		/// Normal vector on plane. Needs to have unit length.
		/// </param>
		SquareImplementation(const Vector3D& normal)
			: m_normal(normal), m_length(2.0)
		{
			assert(normal.is_unit());
		}

		virtual void initialize_hit(Hit* hit, const Ray& ray, double t) const = 0;

	public:
		std::vector<std::shared_ptr<Hit>> find_all_hits(const math::Ray& ray) const override
		{
			std::vector<std::shared_ptr<Hit>> hits;

			// Compute denominator
			double denom = ray.direction.dot(m_normal);

			// If denominator == 0, there is no intersection (ray runs parallel to plane)
			if (denom != approx(0.0))
			{
				// Compute numerator
				double numer = -((ray.origin - Point3D(0, 0, 0)).dot(m_normal));

				// Compute t
				double t = numer / denom;

				// Create hit object
				auto hit = std::make_shared<Hit>();

				// shared_ptr<T>::get() returns the T* inside the shared pointer
				initialize_hit(hit.get(), ray, t);

				Point3D hit_posistion = hit->position;
				//double p_distance = sqrt( pow(p.x(),2) + pow(p.y(), 2) );
				Point2D a = Point2D( -(m_length/2) , (m_length/2) );
				Point2D b = Point2D( (m_length / 2), (m_length / 2));
				Point2D c = Point2D( (m_length / 2), -(m_length / 2));
				Point2D d = Point2D( -(m_length / 2), -(m_length / 2));
				
				Point3D p = hit->position;
				if (m_normal == Vector3D(0, 0, 1)) {
					if (p.x() >= d.x() && p.x() <= c.x() && p.y() >= d.y() && p.y() <= a.y()) {
						// Put hit in list
						hits.push_back(hit);
					}
				}
				else if (m_normal == Vector3D(1, 0, 0)) {
					if (p.z() >= d.x() && p.z() <= c.x() && p.y() >= d.y() && p.y() <= a.y()) {
						// Put hit in list
						hits.push_back(hit);
					}
				}
				
				else if (m_normal == Vector3D(0, 1, 0)) {
					if (p.x() >= d.x() && p.x() <= c.x() && p.z() >= d.y() && p.z() <= a.y()) {
						// Put hit in list
						hits.push_back(hit);
					}
				}
				}

			return hits;
		}

		bool find_first_positive_hit(const math::Ray& ray, Hit* hit) const override
		{
			assert(hit);


			// Compute denominator
			double denom = ray.direction.dot(m_normal);

			// If denominator == 0, there is no intersection (ray runs parallel to plane)
			if (denom != approx(0.0))
			{
				// Compute numerator
				double numer = -((ray.origin - Point3D(0, 0, 0)).dot(m_normal));

				// Compute t
				double t = numer / denom;
				
				if (t > 0) {
					
					if (hit->t && t < hit->t)
					{	
						
						Point2D a = Point2D(-(m_length / 2), (m_length / 2));
						Point2D b = Point2D((m_length / 2), (m_length / 2));
						Point2D c = Point2D((m_length / 2), -(m_length / 2));
						Point2D d = Point2D(-(m_length / 2), -(m_length / 2));
						Point3D p = ray.at(t);
						
						if (m_normal == Vector3D(0, 0, 1)) {
							if (p.x() >= d.x() && p.x() <= c.x() && p.y() >= d.y() && p.y() <= a.y() ) {
								initialize_hit(hit, ray, t);
								return true;

							}
						}
						else if (m_normal == Vector3D(1, 0, 0)) {
							if (p.z() >= d.x() && p.z() <= c.x() && p.y() >= d.y() && p.y() <= a.y()) {
								initialize_hit(hit, ray, t);
								return true;

							}
						}
						else if (m_normal == Vector3D(0, 1, 0)) {
							if (p.x() >= d.x() && p.x() <= c.x() && p.z() >= d.y() && p.z() <= a.y()) {

								initialize_hit(hit, ray, t);
								return true;

							}
						}
						
					}
					
				}
				// No positive hits were found
				return false;
			}
		}
	};

	class SquareXYImplementation : public SquareImplementation
	{
	public:
		SquareXYImplementation()
			: SquareImplementation(Vector3D(0, 0, 1))
		{
			// NOP
		}

		math::Box bounding_box() const override
		{
			auto range = interval(-1.0, 1.0);

			return Box(range, range, interval(-0.01, 0.01));
			
		}

	protected:
		void initialize_hit(Hit* hit, const Ray& ray, double t) const override
		{
			hit->t = t;
			hit->position = ray.at(hit->t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.x(), hit->position.y());
			hit->normal = ray.origin.z() > 0 ? m_normal : -m_normal;
		}
	};
	
	class SquareXZImplementation : public SquareImplementation
	{
	public:
		SquareXZImplementation()
			: SquareImplementation(Vector3D(0, 1, 0))
		{
			// NOP
		}

		math::Box bounding_box() const override
		{
			auto range = interval(-1.0, 1.0);

			return Box(range, interval(-0.01, 0.01), range);
			
		}

	protected:
		void initialize_hit(Hit* hit, const Ray& ray, double t) const override
		{
			hit->t = t;
			hit->position = ray.at(hit->t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.x(), hit->position.z());
			hit->normal = ray.origin.y() > 0 ? m_normal : -m_normal;
		}
	};

	class SquareYZImplementation : public SquareImplementation
	{
	public:
		SquareYZImplementation()
			: SquareImplementation(Vector3D(1, 0, 0))
		{
			// NOP
		}

		math::Box bounding_box() const override
		{
			auto range = interval(-1.0, 1.0);

			return Box(interval(-0.01, 0.01),range, range);
		}

	protected:
		void initialize_hit(Hit* hit, const Ray& ray, double t) const override
		{
			hit->t = t;
			hit->position = ray.at(hit->t);
			hit->local_position.xyz = hit->position;
			hit->local_position.uv = Point2D(hit->position.y(), hit->position.z());
			hit->normal = ray.origin.x() > 0 ? m_normal : -m_normal;
		}
	};
}






	Primitive raytracer::primitives::xy_square()
	{
		return Primitive(std::make_shared<SquareXYImplementation>());
	}

	Primitive raytracer::primitives::xz_square()
	{
		return Primitive(std::make_shared<SquareXZImplementation>());
	}

	Primitive raytracer::primitives::yz_square()
	{
		return Primitive(std::make_shared<SquareYZImplementation>());
	}
