#pragma once

#include "primitives/primitive.h"




namespace raytracer
{
	namespace primitives
	{

		Primitive hiearchical_bounding_box_accelerator(std::vector<Primitive>& children);
	}
}
