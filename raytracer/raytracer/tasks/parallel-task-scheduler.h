#pragma once

#include "tasks/task-scheduler.h"
#include <thread>
#include <iostream>


namespace tasks
{
	namespace schedulers
	{
		
		TaskScheduler parallel();
	}
}
