#include "tasks/parallel-task-scheduler.h"

using namespace tasks;


namespace
{
	
	class ParallelTaskScheduler : public tasks::schedulers::_private_::TaskSchedulerImplementation
	{
	public:
		

		void perform(std::vector<std::shared_ptr<Task>> tasks) const
		{
			std::vector<std::thread> batch;
			for (auto task : tasks)
			{
				batch.push_back(std::thread( &Task::perform, task ));

			}
			for (auto& thread : batch) {
				
				thread.join();
			}
		}
	};
}

TaskScheduler tasks::schedulers::parallel()
{
	return TaskScheduler(std::make_shared<ParallelTaskScheduler>());
}
