#pragma once
#include <string>
#include "primitives\primitive.h"

using namespace raytracer;

namespace mesh
{
	class MeshReader
	{
	public:
		MeshReader(const std::string& filename);

		std::vector<Primitive> read_ply();

	protected:
		

	public:
		

	protected:
		std::string filename;

	};
}