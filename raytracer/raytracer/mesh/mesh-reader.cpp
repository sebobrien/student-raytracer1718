#include "mesh-reader.h"
#include <iostream>
#include<fstream>
#include <vector>
#include "math.h"
#include <algorithm>
#include <sstream>
#include <iterator>
#include "primitives\primitives.h"
#include "primitives\primitive.h"
#include "primitives\triangle-primitive.h"

using namespace std;
using namespace raytracer;



mesh::MeshReader::MeshReader(const std::string&  filename) : filename(filename)
{

}

std::vector<Primitive> mesh::MeshReader::read_ply() {

	ifstream inFile(filename, ios::in);
	string line;
	std::vector<Point3D> vertices;
	std::vector<Primitive> triangles;

	int vertices_n = 0;
	int faces_n = 0;
	int line_count = 0;
	bool end_header = false;
	bool end_faces = false;
	bool end_vertices = false;
	if (!inFile)
	{
		cout << "could not open " << filename << std::endl;
		return triangles;
	}

	while (inFile)
	{
		
		getline(inFile, line);
		
		if (!end_header) {
			if (!line.find("element vertex")) {
				string s = line;
				string s_x = "element vertex ";
				std::string::size_type i = s.find(s_x);
				if (i != std::string::npos)
					s.erase(i, s_x.length());			
				vertices_n = atof(s.c_str());
			}
			if (!line.find("element face")) {
				string s = line;
				string s_x = "element face ";
				std::string::size_type i = s.find(s_x);
				if (i != std::string::npos)
					s.erase(i, s_x.length());
				faces_n = atof(s.c_str());
			}
			if (!line.find("end_header")) {
				end_header = true;
				getline(inFile, line);
				
			}
			

		}

		if (end_header && !end_vertices) {
			
			//cout << "linecount: " << line_count << " " << line << endl;
			string vertex_s = line;

			std::istringstream sstream(vertex_s);
			std::istream_iterator<std::string> beg(sstream), end;

			std::vector<std::string> points(beg, end);

			//vertex_s.erase(std::remove(vertex_s.begin(), vertex_s.end(), ' '), vertex_s.end());

			double x = atof(points[0].c_str());
			double y = atof(points[1].c_str());
			double z = atof(points[2].c_str());
			
			vertices.push_back(Point3D(x, y, z));
			/*cout << Point3D(x, y, z) << endl;*/
			line_count++;
			if (line_count > vertices_n) {
				end_vertices = true;
			}

		}
		
		if (end_header && end_vertices && !end_faces) {
			
			//cout << "linecount2: " << line_count << " "  << line << endl;
			
			string vertex_s = line;

			std::istringstream sstream(vertex_s);
			std::istream_iterator<std::string> beg(sstream), end;

			std::vector<std::string> points(beg, end);
			int p1_i = atof(points[1].c_str());
			int p2_i = atof(points[2].c_str());
			int p3_i = atof(points[3].c_str());
			
			Point3D p1 = vertices[p1_i];
			Point3D p2 = vertices[p2_i];
			Point3D p3 = vertices[p3_i];
			/*cout << p1 << endl;
			cout << p2 << endl;
			cout << p3 << endl;*/
			triangles.push_back(primitives::triangle(p1, p2, p3));		
			line_count++;
			if (line_count > vertices_n + faces_n) {
				
				end_faces = true;
			}
		}
		
		
	}

	inFile.close();
	/*cout << triangles.size() << endl;*/
	return triangles;
	
	
	
	/*cout << vertices.at(0) << endl;
	cout << vertices.back() << endl;*/
	

}
