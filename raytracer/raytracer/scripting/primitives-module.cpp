#ifndef EXCLUDE_SCRIPTING

#include "scripting/primitives-module.h"
#include "scripting/scripting-util.h"
#include "primitives/primitives.h"
#include "math/functions.h"
#include "animation/time-stamp.h"

using namespace chaiscript;
using namespace raytracer;
using namespace math;


namespace
{
    Primitive make_union(const std::vector<chaiscript::Boxed_Value>& boxed_children)
    {
        std::vector<Primitive> children(boxed_children.size());

        std::transform(boxed_children.begin(), boxed_children.end(), children.begin(), [](chaiscript::Boxed_Value boxed) {
            return chaiscript::boxed_cast<Primitive>(boxed);
        });

        return primitives::make_union(children);
    }
	Primitive triangle(const Point3D p1, const Point3D p2, const Point3D p3) {
		return primitives::triangle(p1, p2, p3);
	}
	Primitive BoundingBoxAccelerator(const Primitive child) {
		return primitives::bounding_box_accelerator(child);
	}
	Primitive HBoundingBoxAccelerator(const std::vector<chaiscript::Boxed_Value>& boxed_children) {
		std::vector<Primitive> children(boxed_children.size());

		std::transform(boxed_children.begin(), boxed_children.end(), children.begin(), [](chaiscript::Boxed_Value boxed) {
			return chaiscript::boxed_cast<Primitive>(boxed);
		});
		return primitives::hiearchical_bounding_box_accelerator(children);
	}
	Primitive mesh(std::string filename) {
		std::cout << "hello" << std::endl;
		return primitives::mesh(filename);
	}
}

ModulePtr raytracer::scripting::_private_::create_primitives_module()
{
    auto module = std::make_shared<chaiscript::Module>();

    util::register_type<Primitive>(*module, "Primitive");
    util::register_assignment<Primitive>(*module);

    // Binds helper function defined earlier in this file, exposing the function under the same name
#   define BIND_HELPER_FUNCTION(NAME)                  BIND_HELPER_FUNCTION_AS(NAME, NAME)

    // Binds helper function defined earlier in this file, exposing the function under a different name
#   define BIND_HELPER_FUNCTION_AS(FACTORY, NAME)      module->add(fun(&FACTORY), #NAME)

    // Bypasses helper functions and directly binds to a function from raytracer::primitives
#   define BIND_DIRECTLY(NAME)                         BIND_HELPER_FUNCTION_AS(raytracer::primitives::NAME, NAME)
    BIND_DIRECTLY(sphere);
    BIND_DIRECTLY(xy_plane);
	BIND_DIRECTLY(xz_plane);
	BIND_DIRECTLY(yz_plane);
	BIND_DIRECTLY(xy_square);
	BIND_DIRECTLY(xz_square);
	BIND_DIRECTLY(yz_square);
	BIND_DIRECTLY(xy_circle);
	BIND_DIRECTLY(xz_circle);
	BIND_DIRECTLY(yz_circle);
	BIND_HELPER_FUNCTION(triangle);
    BIND_HELPER_FUNCTION_AS(make_union, union);
	BIND_HELPER_FUNCTION_AS(BoundingBoxAccelerator,bba);
	BIND_HELPER_FUNCTION_AS(HBoundingBoxAccelerator, hbba);
    BIND_DIRECTLY(decorate);
    BIND_DIRECTLY(translate);
	BIND_DIRECTLY(scale);
	BIND_DIRECTLY(rotate_around_x);
	BIND_DIRECTLY(rotate_around_y);
	BIND_DIRECTLY(rotate_around_z);
	BIND_DIRECTLY(mesh);
#   undef BIND_HELPER_FUNCTION_AS
#   undef BIND_DIRECTLY
#   undef BIND_HELPER_FUNCTION

    return module;
}

#endif
